# README #


### This project allows users to perform Pricipal Component Analysis with taxonomer data ###

* This takes taxonomer output excell files, extracts the data, then performs a PCA 
* Version 1.0: This Version only works for Bacterial kingdom

### How do I get set up? ###

* Simply Run the AnalysisInterface Python file
* Select Kingdom to perform PCA on and a file from the directory where your taxonomer results are and start 
* the analysis. This Package comes with a file called Data which can be used to test the program

* Must have Python 3.5, with the following packages: sklearn, pandas, matplotlib, sqlite3, pyqt4, sys, os, numpy
* SQLITE3 Database


### Who do I talk to? ###

* Nick Consiglio nick.consiglio@utah.edu
