from setuptools import setup

setup(name='TaxonomerPCA',
	version='1.0',
	description='Principal Component Anlaysis with Taxonomer Data',
	author='Nick Consiglio',
	author_email='nick.consiglio@utah.edu',
	packages=['sklearn', 'matplotlib', 'pandas', 'os', 'sys','TaxonomerPCA', 'pyqt4', 'time', 'sqlite3','numpy']
	)
