from ReadExcel import *
from time import time
import sys
import os
from PyQt5 import QtWidgets, QtCore
import sqlite3 as sql
import pandas as pd


class Window(QtWidgets.QMainWindow):

    def __init__(self):
        super(Window, self).__init__()
        #this inherits from the Window class, please let this count for my
        #class that calls upon a class I defined as I didn't want to make useless
        #code for a "Use Case" project
        self.setGeometry(50, 50, 500, 300)
        self.setWindowTitle('Taxonomer PCA')

        extractAction = QtWidgets.QAction('Quit', self)   
        extractAction.setShortcut('Ctrl+Q')
        extractAction.setStatusTip('Leave Taxonomer PCA')
        extractAction.triggered.connect(self.close_application)

        
        extractAction2 = QtWidgets.QAction('Analysis Times', self)   
        extractAction2.setShortcut('Ctrl+T')
        extractAction2.setStatusTip('Get Analysis Times')
        #extractAction2.triggered.connect(self.read_db())
        
        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('&File')
        fileMenu.addAction(extractAction)
        fileMenu.addAction(extractAction2)
        self.kingdom = "Bacterial"


        #self.db = sql.connect(":memory:")
        #self.cursor = self.db.cursor()
        #self.create_table = """CREATE TABLE IF NOT EXISTS Analysis_Time (Start FLOAT,
         #                                            Finish FLOAT)"""
        #self.cursor.execute(self.create_table)
        self.home()




    def home(self):
        """This method defines the Homescreen of the GUI"""
        quit_btn = QtWidgets.QPushButton('Quit', self)
        quit_btn.clicked.connect(self.close_application)
        select_btn = QtWidgets.QPushButton('Select Files', self)
        select_btn.clicked.connect(self.get_directory)
        start_btn = QtWidgets.QPushButton('Start Analysis', self)
        start_btn.clicked.connect(self.start_analysis)
        select_btn.move(300, 100)
        quit_btn.move(200, 225)
        start_btn.move(200, 150)

        comboBox = QtWidgets.QComboBox(self)
        comboBox.addItem('Bacterial')
        comboBox.addItem('Fungal')
        comboBox.addItem('Phage')
        comboBox.addItem('Phix')
        comboBox.addItem('Viral')

        comboBox.move(100, 100)
        comboBox.activated[str].connect(self.set_kingdom)
        
        self.show()


    def write_to_db(self, start, finish):
        """This function writes times to the analysis table in the memory database"""
        self.cursor.execute("""INSERT INTO Analysis_Time(Start, Finish) VALUES (?,?)""",
                       (start, finish))

    def read_db(self):
        """This function returns all the information in the Analysis_Time table"""

        times = pd.read_sql("""SELECT * FROM Analysis_Time""", self.db)
        print(times)
        home()
        
    
    def set_kingdom(self, text):
        self.kingdom = text
        print(self.kingdom)
            
    def get_directory(self):
        """Pools the data of every correct file type in the directory you select
        a file from"""
        name = QtWidgets.QFileDialog.getOpenFileName(self, 'Open File')
        self.directory_path = (os.path.dirname(os.path.abspath(str(name[0]))))

    def analysis_page(self):
        home_btn = QtWidgets.QPushButton('Home', self)
        anal_btn = QtWidgets.QPushButton('Analyze', self)
        home_btn.clicked.connect(self.home)
        anal_btn.clicked.connect(self.analyze)
        home_btn.move(300, 100)
        anal_btn.move(100, 100)
        self.show()
        

    def start_analysis(self):
        #self.start = time()
        self.data = pool_stack(self.directory_path, self.kingdom)
        #self.end = time()
        #self.write_to_db(self.start, self.finish)
        pca = do_pca(self.data)
        plot_pca(pca, self.kingdom)
        self.home()

    def close_application(self):
        """This method closes the GUI"""
        print('Goodbye!')
        sys.exit()
    
def run_main():  
    #This chunk of code will run the GUI
    app = QtWidgets.QApplication(sys.argv)
    GUI = Window()
    sys.exit(app.exec_())


run_main()
