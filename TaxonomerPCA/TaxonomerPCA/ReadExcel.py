import pandas as pd
import os
from sklearn import decomposition
import matplotlib.pyplot as plt
import sqlite3 as sql
from time import time

__all__ = ['get_files_in_directory', 'df_from_excel', 'make_kingdom_df',
           'clean_finest_classification', 'stack_dataframes',
           'clean_df','pool_stack','do_pca', 'plot_pca']


def get_files_in_directory(directoryPath):
    """This function takes a filepath to a directory as the input and returns
    a list of paths to each file in the directory"""

    pathList= os.listdir(directoryPath)
    for item in pathList:
        if "~$" in item:
            pathList.remove(item)
    return [os.path.join(directoryPath, item) for item in pathList]

def df_from_excel(filepath):
    '''Read classifier data from excel file into pandas DF'''

    df = pd.read_excel(filepath, sheetname="classifier")
    return df

def make_kingdom_df(df, kingdom):
    """This function returns a dataframe of only the bacterial reads
    from the sample. Input is a dataframe created from the Taxonomer
    excel output
    kingdom is key word is set to bacterial as defualt. must have : after kindom
    """

    df2 = df.loc[df['Taxonomy'].str[0:10].isin([kingdom])]
    return df2

def clean_finest_classification(df):
    """This function cleans up the finest classification column
    of the taxonmer dataframe"""

    col = df.columns.values
    imp = col[0]
    df[imp] = df[imp].str.replace("no rank:", '')
    df[imp] = df[imp].str.replace("species:", '')
    return df



def clean_df(df, read_thresh=1):
    """This function cleans u the the genus dataframe that get_genus_info
       makes"""

    column_names = ['Finest Classification', 'Count', 'Relative Abundence', 'Taxonomy']

    return df[df['Count'] >= read_thresh][column_names]
def normalize_count(df):
    df['Count_sum'] = sum(df['Count'])
    df['Normalized'] = df['Count']/df['Count_sum']
    return df
    

def pool_data(directory, classification):
    """This function will take every file in a directory and turn it into a
    a workable dataframe consisting of the genus's for a patient sample and the counts"""

    files = get_files_in_directory(directory)
    classification = classification+":"
    frames = []
    columns=['Taxonomy', 'Normalized']
    for patient in files:
        if 'full.xlsx' in patient:
            df = df_from_excel(patient)#make dataframe from file
            df = make_kingdom_df(df, classification)#extract bacterial data
            df = clean_finest_classification(df) 
            df = clean_df(df, read_thresh=5)
            df = normalize_count(df)
            df = df[columns]
            df['Sample'] = files.index(patient)
            
            frames.append(df)
            print('Done extracting file '+str(files.index(patient)+1)+' of '+str(len(files)))
    print('Data Pooling Complete')
    return frames

def stack_dataframes(dataframe_list):
    """This function merges the dataframes for PCA into one dataframe using
    the taxonomy classification for the key"""

    new = pd.concat(dataframe_list)
    print('Dataframes Combined')
    return new

def combine_count(df):
    """Used to combine the counts of like taxonomies"""

    return sum(df['Normalized'])

def pool_stack(directory, classification):
    """This function pull taxonomer output files from a directory
    and puts all the classification information needed for a PCA in a dataframe"""


    #stack_start = time()
    print('Starting Pooling')
    df_list = pool_data(directory, classification)
    df = stack_dataframes(df_list)
    df = df[['Sample', 'Taxonomy', 'Normalized']]
    df = df.groupby(['Sample', 'Taxonomy']).apply(combine_count)
    df = pd.DataFrame(df).reset_index()
    df = df.rename(columns={0:'Normalized'})
    df = df.pivot(index='Sample', columns='Taxonomy', values = 'Normalized')
    #stack_finish = time()
    #write_to_db(stack_start, stack_finish)
    df = df.fillna(0)
    print('Dataframe ready for PCA')
    return df
######################################################################################



def do_pca(dataframe):
    """This function preforms a Pricipal component analysis on
    the dataframe it is pass, it reduced each element to two
    pricipal components"""

    redux = [r.values for i,r in dataframe.iterrows()]
    pca = decomposition.PCA(n_components=2)
    pca = pca.fit(redux)
    return pca.transform(redux)

def plot_pca(redux, title = 'Bacterial'):
    """plots data that has been reduced to two
    dimentions with  sklearn.decomposition.PCA"""

    X = [redux[a,0] for a in range(len(redux))]
    Y = [redux[a,1] for a in range(len(redux))]
    fig, ax = plt.subplots(1)
    fig.set_size_inches((5,5))
    plt.scatter(X, Y)
    ax.set_ylabel("Principal Component 1")
    ax.set_xlabel("Pricipal Component 2")
    ax.set_title(title+' Principal Component Analysis')
    plt.show()

################################################################################

#df = pool_stack('C:\\Users\\nicho_000\\Desktop\\EoE\\Taxonomer Full analysis', 'Bacterial')
#redux = do_pca(df)
#plot = plot_pca(redux)
#test = pool_stack('C:\\Users\\nicho_000\\Desktop\\EoE\\Taxonomer Full Test', 'fungal')

